module git.card10.badge.events.ccc.de/q3k/annoyatron

go 1.12

require (
	9fans.net/go v0.0.2 // indirect
	github.com/alecthomas/gometalinter v3.0.0+incompatible // indirect
	github.com/alecthomas/units v0.0.0-20190717042225-c3de453c63f4 // indirect
	github.com/davidrjenni/reftools v0.0.0-20190411195930-981bbac422f8 // indirect
	github.com/fatih/gomodifytags v0.0.0-20190716070638-2dd5565a4bd8 // indirect
	github.com/fatih/motion v0.0.0-20190527122956-41470362fad4 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf // indirect
	github.com/inconshreveable/log15 v0.0.0-20180818164646-67afb5ed74ec
	github.com/josharian/impl v0.0.0-20190715203526-f0d59e96e372 // indirect
	github.com/jstemmer/gotags v1.4.1 // indirect
	github.com/keegancsmith/rpc v1.1.0 // indirect
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/klauspost/asmfmt v1.2.0 // indirect
	github.com/koron/iferr v0.0.0-20180615142939-bb332a3b1d91 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mdempsky/gocode v0.0.0-20190203001940-7fb65232883f // indirect
	github.com/rogpeppe/godef v1.1.1 // indirect
	github.com/stamblerre/gocode v0.0.0-20190327203809-810592086997 // indirect
	github.com/waigani/diffparser v0.0.0-20190426062500-1f7065f429b5
	github.com/xanzy/go-gitlab v0.19.0
	github.com/zmb3/gogetdoc v0.0.0-20190228002656-b37376c5da6a // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/lint v0.0.0-20190409202823-959b441ac422 // indirect
	golang.org/x/mod v0.1.0 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190726091711-fc99dfbffb4e // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190728063539-fc6e2057e7f6 // indirect
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c // indirect
	honnef.co/go/tools v0.0.0-2019.2.1 // indirect
)
