FROM golang:1.12

ADD . /app
RUN set -e -x ;\
    cd /app ;\
    go build

