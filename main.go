package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	log "github.com/inconshreveable/log15"
	gitlab "github.com/xanzy/go-gitlab"
)

var (
	flagToken   string
	flagProject string

	username     string
	git          *gitlab.Client
	semaphores   map[int]chan struct{}
	semaphoresMu sync.Mutex
)

func main() {
	flag.StringVar(&flagToken, "token", "", "Personal Access Token for bot")
	flag.StringVar(&flagProject, "project", "card10/firmware", "GitLab project")
	flag.Parse()

	semaphores = make(map[int]chan struct{})

	if flagToken == "" {
		log.Error("token must be set")
		return
	}

	git = gitlab.NewClient(nil, flagToken)
	git.SetBaseURL("https://git.card10.badge.events.ccc.de/api/v4")

	user, _, err := git.Users.CurrentUser()
	if err != nil {
		log.Error("gitlab failed", "err", err)
		return
	}
	username = user.Username
	log.Info("GitLab seems to work", "username", username)

	log.Info("Starting annoyatron service at :8080...")

	http.HandleFunc("/ping/mr", handlePing)
	http.ListenAndServe(":8080", nil)
}

func handlePing(w http.ResponseWriter, r *http.Request) {
	mr := r.URL.Query().Get("mr")
	if mr == "" {
		http.NotFound(w, r)
		return
	}

	mrn, err := strconv.Atoi(mr)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	err = inspect(r.Context(), mrn)
	if err != nil {
		log.Error("inspect failed", "mr", mr, "err", err)
		w.WriteHeader(500)
		fmt.Fprintf(w, "could not annoy :(")
		return
	}

	w.WriteHeader(200)
	fmt.Fprintf(w, "beep boop, you have been annoyed.")
}

func inspect(ctx context.Context, mr int) error {
	l := log.New("mr", mr)
	l.Info("Inspecting...")

	mro, _, err := git.MergeRequests.GetMergeRequest(flagProject, mr, &gitlab.GetMergeRequestsOptions{}, gitlab.WithContext(ctx))
	if err != nil {
		return err
	}

	if mro.State != "opened" {
		return fmt.Errorf("MR is %q, want active", mro.State)
	}

	//if mro.Pipeline.Status != "running" {
	//	return fmt.Errorf("MR pipeline state is %q, want running", mro.Pipeline.Status)
	//}

	go supervise(context.Background(), l, mr)

	return nil
}

func supervise(ctx context.Context, l log.Logger, mr int) {
	semaphoresMu.Lock()
	if _, ok := semaphores[mr]; !ok {
		semaphores[mr] = make(chan struct{}, 1)
	}
	sem := semaphores[mr]
	semaphoresMu.Unlock()

	sem <- struct{}{}
	l.Info("Supervising...")
	defer func() {
		<-sem
	}()

	start := time.Now()
	t := time.NewTicker(time.Second * 5)
	failures := 0
	var mro *gitlab.MergeRequest
	var err error

	for {
		select {
		case <-ctx.Done():
			return
		case <-t.C:
			break
		}

		if time.Since(start) > time.Second*60*5 {
			l.Error("Supervision failed, pipeline build time elapsed.")
			return
		}
		if failures > 10 {
			l.Error("Supervision failed, GitLab broke.")
			return
		}

		mro, _, err = git.MergeRequests.GetMergeRequest(flagProject, mr, &gitlab.GetMergeRequestsOptions{}, gitlab.WithContext(ctx))
		if err != nil {
			failures += 1
			log.Warn("Supervision GitLab check failed", "err", err)
			continue
		}
		if mro.Pipeline.Status != "success" && mro.Pipeline.Status != "failed" {
			continue
		}

		var lintState = "unknown"
		var lintURL = ""

		jobs, _, err := git.Jobs.ListPipelineJobs(mro.SourceProjectID, mro.Pipeline.ID, &gitlab.ListJobsOptions{}, gitlab.WithContext(ctx))
		if err != nil {
			failures += 1
			log.Warn("Supervision GitLab check failed", "err", err)
			continue
		}

		for _, job := range jobs {
			if job.Name == "lint" {
				lintState = job.Status
				lintURL = job.WebURL
			}
		}

		if lintState == "success" {
			if err = reportOkay(ctx, l, mr); err != nil {
				failures += 1
				log.Warn("Reporting to GitLab failed", "err", err)
				continue
			}
			return

		} else if lintState == "failed" {
			if err = reportBroken(ctx, l, mr, lintURL); err != nil {
				failures += 1
				log.Warn("Reporting to GitLab failed", "err", err)
				continue
			}
			return
		} else {
			if err = reportUnknown(ctx, l, mr); err != nil {
				failures += 1
				log.Warn("Reporting to GitLab failed", "err", err)
				continue
			}
			return
		}
	}
}

func reportUnknown(ctx context.Context, l log.Logger, mr int) error {
	l.Info("Reporting unknown")
	return report(ctx, l, mr, `I can't seem to find a 'lint' job in the pipeline that just ran. I don't know what's going on. I'm scared.`)
}

func reportOkay(ctx context.Context, l log.Logger, mr int) error {
	l.Info("Reporting okay")
	return report(ctx, l, mr, `Looks good to me! Thanks for keeping our codebase clean.
	
However, I'm only a bot - so a human will still have to approve this request.`)
}

func reportBroken(ctx context.Context, l log.Logger, mr int, url string) error {
	l.Info("Reporting broken")
	return report(ctx, l, mr, fmt.Sprintf((`Sorry, it looks like this Merge Request has some code quality issues!

The pipeline [lint](%s) has failed - look at its failure output to understand what sort of diffs we'd like you to apply.

You can also use `+"`tools/code-style.sh`"+` to fix files that have issues.

Good luck! I will update this comment when I detect you have applied your fixes.`), url))
}

func report(ctx context.Context, l log.Logger, mr int, body string) error {
	notes, _, err := git.Notes.ListMergeRequestNotes(flagProject, mr, &gitlab.ListMergeRequestNotesOptions{})
	if err != nil {
		return err
	}

	keeping := false
	for _, note := range notes {
		if note.Author.Username == username {
			if note.Body == body {
				keeping = true
				continue
			}
			log.Info("Deleting existing note", "nid", note.ID)
			git.Notes.DeleteMergeRequestNote(flagProject, mr, note.ID, gitlab.WithContext(ctx))
		}
	}

	if keeping {
		log.Info("Keeping old note")
		return nil
	}
	log.Info("Adding new note")
	_, _, err = git.Notes.CreateMergeRequestNote(flagProject, mr, &gitlab.CreateMergeRequestNoteOptions{Body: &body}, gitlab.WithContext(ctx))
	return err
}
